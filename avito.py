from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup
import time
import os
import csv
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
import pymorphy2
from datetime import datetime
import locale


options = webdriver.ChromeOptions()
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_argument('--ignore-certificate-errors')
options.add_argument('--ignore-ssl-errors')
options.add_argument('--ignore-certificate-errors-spki-list')
options.add_experimental_option('excludeSwitches', ['enable-logging'])



  


def get_links():
                        
    open ('links_ads.txt', 'w') 

    with open("old_links_ads.txt", encoding='utf-8') as file:
        old_link_ads = file.read().splitlines()
    
    driver = webdriver.Chrome(
            executable_path="chromedriver.exe",options=options)  
    driver.get(url='https://www.avito.ru/kirovskaya_oblast/kvartiry/prodam-ASgBAgICAUSSA8YQ?cd=1')
    driver.maximize_window()
    time.sleep(2)

    soup = BeautifulSoup(driver.page_source, 'lxml')

    find_number_last_page = soup.find_all('span', class_='styles-module-text-InivV')
    print(find_number_last_page[-1].text)
    

    for number_page in range(1, int(find_number_last_page[-1].text) +1):
        

        try:
            url_page = f"https://www.avito.ru/kirovskaya_oblast/kvartiry/prodam-ASgBAgICAUSSA8YQ?cd=1&p={number_page}"
            print(url_page)
            driver.get(url_page)
            time.sleep(7)
            soup_collect_links = BeautifulSoup(driver.page_source, 'lxml')
        except:
            soup_collect_links = ''

        try:
            items = soup_collect_links.find_all('div', class_='iva-item-root-_lk9K')
        
        except:
            items = ''

        try:
            
            for item in items:
                link = 'https://www.avito.ru' + item.find('a').get('href')
                if link not in old_link_ads:
                    print(link)
                    with open('links_ads.txt', '+a', encoding='utf-8') as f:
                                    f.write(f"{link}\n") 
                    with open('old_links_ads.txt', '+a', encoding='utf-8') as f:
                                    f.write(f"{link}\n") 
        except:
            pass
        time.sleep(1)


def get_data():
        driver = webdriver.Chrome(
            executable_path="chromedriver.exe",options=options)  
        driver.maximize_window()    

        open('result.csv', 'w')

        with open('links_ads.txt', 'r', encoding='utf-8') as f:
                urls_avito = f.read().splitlines()
        
        test_total_area = 'Общая площадь:'
        test_living_area = 'Жилая площадь:'
        test_floor = 'Этаж:'


        cordinates_resort = '67.61006400322123, 33.69542271534026'
        
        
        total_area = ''
        living_area = ''
        floor = ''
      
        
        for url in urls_avito:
                
                try:
                    driver.get(url)
                    time.sleep(3)
                    soup = BeautifulSoup(driver.page_source, 'lxml')
                except:
                    soup = ''
                print(url)
                try:
                      get_all_class_data = soup.find_all('li', class_='params-paramsList__item-appQw')
                except:
                      get_all_class_data = ''

                try:
                      for one_data in get_all_class_data:
                            one = one_data.text

                            if test_total_area in one:
                                  total_area = one.strip('Общая площадь:')
                            
                            
                            if test_living_area in one:
                                  living_area = one
                            
                            
                            if test_floor in one:
                                  floor = one          
                except:
                      pass
                
                try:
                    adress = soup.find('div', itemprop='address').text.strip()
                except:
                    adress  = ''
                test_yesterday = 'вчера'  
                test_today = 'сегодня'  
                try:
                      
                    find_all_data_public = soup.find('article', class_='style-item-footer-text-LEjEe').find('p', class_='styles-module-root-_KFFt').find_all('span')
                    test_data_public = find_all_data_public[-4].text.strip()
                    if test_yesterday in test_data_public:
                          now = datetime.now().day - 1
                          data_public = f"{now}-{datetime.now().month}-2023"
                    elif test_today in test_data_public:
                          now = datetime.now().day 
                          data_public = f"{now}-{datetime.now().month}-2023"
                    else:
                        data_month = find_all_data_public[1].text.split()[2]
                        data_day = find_all_data_public[1].text.split()[1]
                        morph = pymorphy2.MorphAnalyzer()
                        month =  morph.parse(data_month)[0].normal_form.capitalize()
                        change_data_public = f"{data_day}-{month}-2023"
                        locale.setlocale(locale.LC_ALL, 'ru_RU')
                        a_l_data_public = datetime.strptime(change_data_public, "%d-%b-%Y")
                        data_public = str(a_l_data_public).replace(' 00:00:00','')

                except:
                    data_public = ''  
               
                try:
                    get_cordinate_lat = soup.find('div', class_='style-item-map-wrapper-ElFsX style-expanded-x335n').get('data-map-lat')
                    get_cordinate_lon = soup.find('div', class_='style-item-map-wrapper-ElFsX style-expanded-x335n').get('data-map-lon')
                    cordinate_apartment = f"{get_cordinate_lat}, {get_cordinate_lon}"
                except:
                    cordinate_apartment = ''
                try:
                    get_distance = geodesic(cordinate_apartment, cordinates_resort).kilometers
                    distance = f"{get_distance:.2f} км"
                except:
                    distance = ''
                      

                with open(f"result.csv", "a", encoding='utf-8', newline='') as file:
                            writer = csv.writer(file,delimiter=';')
                            writer.writerow((
                                url,
                                total_area,
                                living_area,
                                floor,
                                adress,
                                data_public,
                                distance

                                        
                                                                ))
def main():
    
    get_links()
    get_data()
    

if __name__ == '__main__':
    main()
